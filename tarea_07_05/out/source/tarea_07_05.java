import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.*; 
import java.util.Random; 
import static java.lang.System.out; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class tarea_07_05 extends PApplet {





Circulo tmp;
List<Circulo> lista;
boolean ban = false;
PImage im;
PImage nave;
int x, y;
String id = "";
float xOffset = 0.0f;
float yOffset = 0.0f;

public void setup(){
    
    generacionCirculos();
}

public void draw(){
    background(0,0,0);
    for (int i = 0; i < lista.size(); ++i) {
        lista.get(i).dibujar();
    }
    textSize(24);
    fill(0xffDB145D);
    text(id, 40, 50);
    fill(255);
}

public void generacionCirculos(){
    lista = new ArrayList<Circulo>();
    Random r = new Random();
    int valorDado = 0;
    do{
        valorDado = r.nextInt(50)+1;
    }while (valorDado < 30);

    for (int i = 0; i < valorDado; ++i) {
        int ancho = r.nextInt(width);
        int altura = r.nextInt(height);
        lista.add(new Circulo(ancho, altura, 50, i));
    }    
}

public void mousePressed(){
    id= "";
    if(id != ""){
        lista.get(PApplet.parseInt(id)).setMovimiento(false);
    }
    for (int i = lista.size()-1; i >= 0; i--) {
        if(lista.get(i).isAdentro(mouseX, mouseY)){
            ban = true;
            tmp = lista.get(i);
            id = str(lista.get(i).getId());
            lista.remove(i);
            xOffset = mouseX-tmp.getX(); 
            yOffset = mouseY-tmp.getY(); 
            break;
        }
    }
    if(ban){
        burbuja();
    }
}

public void burbuja(){
    int actual = PApplet.parseInt(id);
    for (int i = PApplet.parseInt(id); i < lista.size(); ++i) {
        lista.get(i).setId(actual);
        actual++;
    }
    tmp.setId(actual);
    lista.add(tmp);
    id = str(actual);
}

public void mouseReleased() {
    ban = false;   
    for (int i = 0; i < lista.size(); i++) {
        lista.get(i).setMovimiento(false);
    }
    id = "";
}

public void mouseDragged() {
    if(ban){
        lista.get(PApplet.parseInt(id)).setX(mouseX-xOffset); 
        lista.get(PApplet.parseInt(id)).setY(mouseY-yOffset); 
    }
}
class Circulo{
    float x;
    float y;
    float diametro;
    int id;
    boolean movimiento; 

    Circulo(float a, float b, float c, int d){
        x=a;
        y=b;
        diametro=c;
        id=d;
        ellipseMode(CENTER);
        movimiento = false; 
    }

    public void dibujar(){
        ellipse(x, y, diametro, diametro);
    }

    public float getX(){
        return x;
    }

    public float getY(){
        return y;
    }

    public float getDiametro(){
        return diametro;
    }

    public int getId(){
        return id;
    }

    public void setX(float x){
        this.x = x;
    }

    public void setY(float y){
        this.y = y;
    }

    public void setDiametro(float diametro){
        this.diametro = diametro;
    }

    public void setMovimiento(boolean movimiento){
        this.movimiento = movimiento;
    }

    public void setId(int id){
        this.id = id;
    }

    public boolean isAdentro(float a, float b){
        if(dist(a, b, x, y) < diametro/2){
            movimiento = true;
            return true;
        }
        return false;
    }

}
  public void settings() {  size(300, 300); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "tarea_07_05" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
