import java.util.*;
import java.util.Random;
import static java.lang.System.out;

Circulo tmp;
List<Circulo> lista;
boolean ban = false;
PImage im;
PImage nave;
int x, y;
String id = "";
float xOffset = 0.0;
float yOffset = 0.0;

void setup(){
    size(300, 300);
    generacionCirculos();
}

void draw(){
    background(0,0,0);
    for (int i = 0; i < lista.size(); ++i) {
        lista.get(i).dibujar();
    }
    textSize(24);
    fill(#DB145D);
    text(id, 40, 50);
    fill(255);
}

void generacionCirculos(){
    lista = new ArrayList<Circulo>();
    Random r = new Random();
    int valorDado = 0;
    do{
        valorDado = r.nextInt(50)+1;
    }while (valorDado < 30);

    for (int i = 0; i < valorDado; ++i) {
        int ancho = r.nextInt(width);
        int altura = r.nextInt(height);
        lista.add(new Circulo(ancho, altura, 50, i));
    }    
}

void mousePressed(){
    id= "";
    if(id != ""){
        lista.get(int(id)).setMovimiento(false);
    }
    for (int i = lista.size()-1; i >= 0; i--) {
        if(lista.get(i).isAdentro(mouseX, mouseY)){
            ban = true;
            tmp = lista.get(i);
            id = str(lista.get(i).getId());
            lista.remove(i);
            xOffset = mouseX-tmp.getX(); 
            yOffset = mouseY-tmp.getY(); 
            break;
        }
    }
    if(ban){
        burbuja();
    }
}

void burbuja(){
    int actual = int(id);
    for (int i = int(id); i < lista.size(); ++i) {
        lista.get(i).setId(actual);
        actual++;
    }
    tmp.setId(actual);
    lista.add(tmp);
    id = str(actual);
}

void mouseReleased() {
    ban = false;   
    for (int i = 0; i < lista.size(); i++) {
        lista.get(i).setMovimiento(false);
    }
    id = "";
}

void mouseDragged() {
    if(ban){
        lista.get(int(id)).setX(mouseX-xOffset); 
        lista.get(int(id)).setY(mouseY-yOffset); 
    }
}
