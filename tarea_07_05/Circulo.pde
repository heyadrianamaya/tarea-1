class Circulo{
    float x;
    float y;
    float diametro;
    int id;
    boolean movimiento; 

    Circulo(float a, float b, float c, int d){
        x=a;
        y=b;
        diametro=c;
        id=d;
        ellipseMode(CENTER);
        movimiento = false; 
    }

    void dibujar(){
        ellipse(x, y, diametro, diametro);
    }

    float getX(){
        return x;
    }

    float getY(){
        return y;
    }

    float getDiametro(){
        return diametro;
    }

    int getId(){
        return id;
    }

    void setX(float x){
        this.x = x;
    }

    void setY(float y){
        this.y = y;
    }

    void setDiametro(float diametro){
        this.diametro = diametro;
    }

    void setMovimiento(boolean movimiento){
        this.movimiento = movimiento;
    }

    void setId(int id){
        this.id = id;
    }

    boolean isAdentro(float a, float b){
        if(dist(a, b, x, y) < diametro/2){
            movimiento = true;
            return true;
        }
        return false;
    }

}